# Workflow for generating vessel detections and survey effort from satellite imagery collected by RADARSAT-2 (Synthetic Aperture Radar) in R

__Main authors:__  Beatrice Proudfoot, Lily Burke   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: beatrice.proudfoot@dfo-mpo.gc.ca, lily.burke@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Software](#software)
  + [Directory Structure](#directory-structure)
- [Caveats](#caveats)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective

An automated workflow for generating RADARSAT vessel detection observations and survey effort from email reports provided by MDA Geospatial Services. 

## Summary

Vessel tracking data shed light on a variety of human pressures within marine conservation areas and are valuable for evaluating the effectiveness of vessel- and fishing-related regulations. The satellite imaging program, RADARSAT-2 (RADARSAT), was launched in 2007 and is used for maritime surveillance, vessel traffic and environmental monitoring, and resource and disaster management (CSA, 2017). Burke et al. (2022) compiled and analyzed RADARSAT data to demonstrate how these data can be used for human pressure monitoring and management effectiveness evaluation in marine conservation areas. The R scripts found in this project were developed to produce a RADARSAT vessel detection dataset as well as provide information on RADARSAT survey effort. 

RADARSAT vessel detections: Images from RADARSAT are analyzed by MDA Ltd. and data are provided in MSOCW DFO MPA Acquisition email reports. Zipped folders are attached in email reports that include Google Earth files (.kml), Over-The-Horizon (OTH) gold text files (.txt), and ASI Association text files (.oth). These files detail vessel observations, including the time and location, MMSI number if the vessel was transmitting AIS, vessel length, and confidence of vessel detection as image analysis may have false positives from waves or rocks.

RADARSAT survey effort: Information on planned Department of National Defence RADARSAT ship detection acquisitions are provided in RS2 Monitoring of MSOC West DFO MPAs email reports. Zipped folders are attached in email reports that include Google Earth files (.kml) and image files (.jpg) of RADARSAT collection plans. These files are used to determine the temporal and spatial resolution of the RADARSAT data and to quantify sampling effort. 

## Status

Ongoing-improvements

## Contents

All scripts described below run from the ~/code working directory. Other files in directory are sourced in the scripts described below and shown in the Directory Structure.

**Radarsat1_datapreparation.R**    
  *  Extracts files from RADARSAT acquistion and monitoring email reports  
  *  Before executing, adjust Outlook folder name and working directory in script and create directories noted in script where outputs will be placed
  *  Outputs will be placed in new directories

**Radarsat2_vesseldetections.R**    
  *  Extracts vessel location information from RADARSAT acquisition email report files
  *  Before executing, adjust working directory

**Radarsat3_surveyeffort.R**    
  *  Extracts RADARSAT collection plan information from monitoring email reports 
  *  Before executing, adjust working directory

## Methods

**Data Preparation**    
*  Filters Outlook floders for RADARSAT acquisition (i.e., vessel detections) and monitoring (i.e., survey effort) email reports
*  Downloads zipped folders in email reports, unzips the folders
*  Extracts files in unzipped folders with information on RADARSAT vessel detections and survey effort

**Vessel Detections**    
*  Loops through OTH Gold text files and extracts RADARSAT vessel detections
*  Loops through SASSI OTH files and extracts AIS association information
*  A .csv located in /*project-name*/data with vessel information that includes the time and location, MMSI number if the vessel was transmitting AIS, vessel length, and confidence of vessel detection as image analysis may have false positives from waves or rocks.

**Survey Effort**    
*  Loops through kml files representing planned satellite swaths and extracts swath information
*  A .csv located in /*project-name*/data with information on RADARSAT swaths such as start time, end time, beam settings, satellite direction. Each swath has a unique SwathID.

## Requirements

### Software
* R version 4.0.2 or newer (https://www.r-project.org/)

### Directory Structure
*  If the name is within <>, it can be changed

```
/---<RadarSat project-name>   
    /---code   
    | 	Radarsat1_datapreparation.R
    | 	Radarsat2_vesseldetections.R
    | 	Radarsat3_surveyeffort.R
    | 	       
    /---data
    | 	<RS_survey_effort>.csv 
    |	<RS_master_survey_effort>.csv
    |	<RS_vessel_detections>.csv
    |	<RS_master_vessel_detections>.csv
    |
 	/---survey_effort 
		/---collection_plan_JPG_files
		|	<date>.jpg
		|
		/---collection_plan_KML_files
		|	<date range to DFO MPA-KML>.kml
		|
		/---unzip_attachments	
		|	  
                /---zip_attachments                
                |       <DFO MPA Image Files date>.zip   
                |       <date range to DFO MPA-KML>.zip   
		|
        /---vessel_detections  
		/---KML_files
		|	<acquisition number or date acquisition number>.kml
		|
		/---OTH_GOLD_files
		|	<date detection resoltuion swath>.txt
		|
		/---SASSI_OTH_files
		|	<date acquisition number>.oth
		|
		/---unzip_attachments	
		|	  
                /---zip_attachments                
                |       <acquisition number>.zip     

   
```

## Caveats

## Acknowledgements

MDA Ltd., Department of National Defence, Marine Security Operations Centre and the following individuals: Georgia Clyde, Josephine Iacarella, Emily Rubidge, B. Thexton, J. Prior, B. Merchant, P. Hagell

## References

Burke, L., Clyde, G., Rubidge, E.M., Proudfoot, B., and Iacarella, J.C. 2022. Monitoring Pacific marine conservation area effectiveness using aerial and RADARSAT-2 (Synthetic Aperture Radar) vessel detection. Can. Tech. Rep. Fish. Aquat. Sci. 3479: xi + 50 p.

CSA, 2017. RADARSAT-2 [WWW Document]. URL https://www.asc-csa.gc.ca/eng/satellites/radarsat2/default.asp (accessed 1.15.21).

Iacarella, J.C., Clyde, G., Dunham, A., 2020. Vessel Tracking Datasets For Monitoring Canada’s Conservation Effectiveness. Can. Tech. Rep. Fish. Aquat. Sci. 3387.

Proudfoot, Beatrice. 2020. Synthetic Aperature Radar Data Compilation - Protocol for Extracting Radar Transmitted Data to Support MPA Network Monitoring in the Pacific Region. Report prepared for Josephine Iacarella, Marine Spatial Ecology and Analysis Section, DFO. 
