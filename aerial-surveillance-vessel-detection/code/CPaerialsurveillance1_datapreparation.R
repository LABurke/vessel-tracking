###############################################################################
#
# Author:       Lily Burke
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      lily.burke@dfo-mpo.gc.ca
#
# Overview: 
# Step 1 of generating vessel detections from Conservation & Protection Aerial
# Surveillance Program (C&P ASP) flyovers.

# Loops through the C&P ASP reports and extracts the Mission date (YYYY-MM-DD) 
# the flyover took place on. Renames the C&P ASP report file name by pasting 
# the Mission date to the file name. The Mission date in the file name is used 
# to extract vessel and mission event information from same flyover in 
# 2_CP_vesseldetections.R. 
#
# Requirements:
# i) create a reports folder in the CP_ASP_flyovers folder where flyover reports
# are saved
# ii) create a data folder in CP_ASP_flyovers folder where vessel detections 
# and all mission events csvs are saved
#
# Citation: Burke, L., Clyde, G., Proudfoot, B., Rubidge, E.M., and 
# Iacarella, J.C. 2022. Monitoring Pacific marine conservation area 
# effectiveness using aerial and RADARSAT-2 (Synthetic Aperture Radar)
# vessel detection. Can. Tech. Rep. Fish. Aquat. Sci.3479: xi + 50 p.
#
################################################################################

# Make library available
library(readxl)
# Set the system locale because of incompatibility of system locale and some 
# reports had invalid strings
Sys.setlocale("LC_ALL", "C")
# ------------------------------------------------------------------------------
# Access directory where C&P ASP reports are saved

dirs <- list.dirs("G:/CP_ASP_flyovers/reports")

# Set working directory and folder path name
for (i in dirs) {
  setwd(i)
  folder <- paste0(getwd(), "/")
  print(folder)
  options(warn = 2)
}

# ------------------------------------------------------------------------------
# Report 1: Mission targets Report

missions <- list.files(dirs, pattern = "*MissionTargetsList*")

for (m in missions) {
  mission.df <- read.csv(m, sep = ",")
  date <- unique(as.Date(mission.df$Time, tryFormats = c("%d/%m/%Y", "%Y%m%d")))
  date <- date[1]
  file.rename(from = paste0(folder, m), to = paste0(folder, date, "_", m))
}

# ------------------------------------------------------------------------------
# Reports 2 - 5: Target, Fishing Vessel AIS, Fishing Vessel Manual On Top, 
# Commercial Vessel AIS

vessels <- list.files(dirs, pattern = "*.xls")

for (v in vessels) {
  df <- read_excel(v)
  date <- df[5, 1]
  date <- gsub(".*: ","", date)
  date <- gsub("\\T.*","", date)
  file.rename(from = paste0(folder, v), to = paste0(folder, date, "_", v))
}

# ------------------------------------------------------------------------------
# Report 6: Aircraft tracks

air <- list.files(dirs, pattern = ("*AircraftTracks*"))

for (a in air) {
  air.df <- read.csv(a, sep = ",")
  date <- unique(as.Date(air.df$X.Time, tryFormats = c("%d/%m/%Y", "%Y%m%d")))
  date <- date[1]
  file.rename(from = paste0(folder, a), to = paste0(folder, date, "_", a))
}

# Remove characters within parentheses of file name including space in front 
# of parentheses 
files <- list.files()
new_filename <- sub('\\ \\(.*\\)', '', files)
file.rename(files, new_filename)


# Files and folders are now prepared for running the 2_CP_vesseldetections.R
